import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int[] largestPermutation(int k, int[] arr) {
        // Complete this function
	// We can assume that if N=5, we'll have exactly 5 numbers in the arr, so N can be retrieving
	// the size from the array.
	//System.out.println("The length is "+arr.length);
	//
	int l=arr.length;
	int permsDone=0;
	int currentHighest=l;
	int position=0;
	for(int perms=k;perms>0;perms--)
	{
		// Produce a permutation and find if it is the largest one.
		// The easiest way to do this would be to find the largest number available
		// and put it in the first position. If the largest number is already there,
		// I don't need to do anything.
		if(arr[position]!=currentHighest)
		{
			// I have to find the highest in the rest of the array.
			if(position<l)
			{
				boolean found=false;
				int search=position+1;
				while(!found && search<l)
				{
					if(arr[search]==currentHighest)
					{
						// produce the swap
						int a=arr[position];
						arr[position]=arr[search];
						arr[search]=a;
						permsDone++;
						found=true;
						// Decrease the current highest.
						currentHighest--;
					}
					search++;
				}
			}
		}
		else
		{
			// The currentHighest is at the first position. Leave it and decrese the current highest.
		}
		// Move to the next position
		position++;
	}
	return arr;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int[] result = largestPermutation(k, arr);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
}
