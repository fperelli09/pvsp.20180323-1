process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function largestPermutation(k, arr, n) {
    var g = n;
    var i = 0;
    while(k > 0 && g > 0) {
        var pos = arr.indexOf(g, i);

        if (i !== pos) {
            var temp = arr[i];
            arr[i] = g;
            arr[pos] = temp;
            k--;
        }

        g--;
        i++;
    }

    return arr;
}


function main() {
    var n_temp = readLine().split(' ');
    var n = parseInt(n_temp[0]);
    var k = parseInt(n_temp[1]);
    arr = readLine().split(' ');
    arr = arr.map(Number);
    var result = largestPermutation(k, arr, n);
    console.log(result.join(" "));


}

