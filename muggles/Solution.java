package hackerrank.challenges;
import java.util.*;
class Solution {
    static int[] largestPermutation(int k, int[] arr) {
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        for (int i=0 ; i<arr.length ; i++) {
            treeMap.put(arr[i], i);
        }
        //NavigableSet nav = treeMap.descendingKeySet();
        Iterator<Integer> it = treeMap.descendingKeySet().iterator();
        for (int j=0 ; j < k ; j++) {
            Integer max = (Integer) it.next();
            int idx = treeMap.get(max);
            if (idx == j) continue;
            swap(arr, j, idx);
        }
        return arr;
    }
    private static void swap(int[] arr, int a, int b) {
        int aux = arr[a];
        arr[a] = arr[b];
        arr[b] = aux;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int arr[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            arr[a_i] = in.nextInt();
        }
        System.out.println(Arrays.toString(largestPermutation(k, arr)));
    }
}